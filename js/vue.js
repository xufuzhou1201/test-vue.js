var input = new Vue({
  el: "#input",
  data:{
    surname: '',
    name: '',
    age: '',
    see: false,
    showSurname: '',
    showName: '',
    showAge: ''
  },
  methods:{
    showData: function (event) {
        // console.log(this.surname);
        // console.log(this.name);
        // console.log(this.age);
        let reg = /^[\d]+$/g;
        let ageTrue = reg.test(this.age);
        // console.log(ageTrue);
        if(ageTrue == true){
          if(this.name != '' && this.surname != '' && this.age != ''){
            this.showSurname = this.surname;
            this.showName = this.name;
            this.showAge = this.age;
            this.surname = '';
            this.name = '';
            this.age = '';
            this.see = true
          }
          else {
            alert("заполните все поля");
          }
        }
        else{
          alert("укажите возраст цифрами");
        }
    }
  }
})
